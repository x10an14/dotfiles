# My Dotfiles
## What I use:
* sway / Waybar
* kitty (only way to get ranger image previews on wayland) 
* fish (figth me!)
* nvim / vim-lsp (pure vimscript)
* qutebrowser
* zathura
* mpd (optionally playerctl if installed)
* mpv (sponsorblock)
* btwOs

## Install:
Most files need to be pre-prossesed with dotdrop.    
By default mapped to Dvorak, edit config.yaml to remap.
```bash
pip install --user dotdrop
dotdrop -c config.yaml install
```
![image.png](./image.png)
